package infernal.grapheditor.view

import java.io.Serializable

/**
 * Created by xxinf on 13-Nov-16.
 */
class Edge(val from: Vertex,
           val to: Vertex)
    : Focusable, Serializable {

    private var focused: Boolean = false
    var weight = 1

    fun x1(): Float = from.x

    fun x2(): Float = to.x

    fun y1(): Float = from.y

    fun y2(): Float = to.y

    override fun focus() {
        focused = true
    }

    override fun unfocus() {
        focused = false
    }

    override fun isFocused(): Boolean {
        return focused
    }

    fun weightPosition(): Pair<Float, Float> {
        val angle = angle()
        return Pair(((to.x + from.x) / 2 + (Math.cos(angle + Math.PI / 2) * 50)).toFloat(), ((to.y + from.y) / 2 + (Math.sin(angle + Math.PI / 2) * 50)).toFloat())
    }

    fun angle(): Double{
        val cos = (to.x - from.x) / Math.sqrt(Math.pow((to.x - from.x).toDouble(), 2.0) + Math.pow((to.y - from.y).toDouble(), 2.0))
        return Math.acos(cos)
    }
}