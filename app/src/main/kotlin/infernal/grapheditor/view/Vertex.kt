package infernal.grapheditor.view

import java.io.Serializable

/**
 * Created by xxinf on 13-Nov-16.
 */
class Vertex(var x: Float,
             var y: Float,
             val id: Int)
    : Focusable, Serializable {

    private var focused: Boolean = false
    var text: String = id.toString()

    override fun focus() {
        focused = true
    }

    override fun unfocus() {
        focused = false
    }

    override fun isFocused(): Boolean {
        return focused
    }

    fun move(dx: Float, dy: Float) {
        x += dx
        y += dy
    }
}