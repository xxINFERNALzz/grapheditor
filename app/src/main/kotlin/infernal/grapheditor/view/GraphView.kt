package infernal.grapheditor.view

import java.io.Serializable

/**
 * Created by xxinf on 13-Nov-16.
 */
class GraphView(var directedEdges: Boolean,
                var vertexesRadius: Float
) : Serializable {

    val vertexes: MutableList<Vertex> = mutableListOf()
    val edges: MutableList<Edge> = mutableListOf()
    @Transient var highLights: MutableList<Edge>? = mutableListOf()

    fun gVertexes(): MutableList<Vertex> = vertexes

    fun gEdges(): MutableList<Edge> = edges

    fun addVertex(vertex: Vertex) = vertexes.add(vertex)

    fun addEdge(edge: Edge) = edges.add(edge)

    fun removeVertex(vertex: Vertex) = vertexes.remove(vertex)

    fun removeEdge(edge: Edge) = edges.remove(edge)


    companion object{
        @JvmStatic val EDGE_SELECTIN_WIDTH = 50
    }

}