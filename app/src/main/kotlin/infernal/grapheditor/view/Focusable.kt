package infernal.grapheditor.view

/**
 * Created by xxinf on 13-Nov-16.
 */
interface Focusable {
    fun focus()
    fun unfocus()
    fun isFocused(): Boolean
}