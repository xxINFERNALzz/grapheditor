package infernal.grapheditor

import infernal.grapheditor.model.Algorithms
import infernal.grapheditor.model.GraphModel
import infernal.grapheditor.view.Edge
import infernal.grapheditor.view.Focusable
import infernal.grapheditor.view.GraphView
import infernal.grapheditor.view.Vertex
import java.io.Serializable
import java.util.*

/**
 * Created by xxinf on 13-Nov-16.
 */
class GraphController() : Serializable {
    //vertexCounter
    private var idUniqueVertex: Int = 0
    val graphModel = GraphModel(true)
    val graphView = GraphView(true, GraphDrawer.VERTEX_RADIUS)

    @Transient private var undoDeque: Deque<Action> = ArrayDeque<Action>()
    @Transient private var redoDeque: Deque<Action> = ArrayDeque<Action>()

    @Transient private var selected: Focusable? = null

    fun isDirected(): Boolean = graphModel.isDirected

    fun reInit() {
        if (undoDeque == null)
            undoDeque = ArrayDeque<Action>()
        if (redoDeque == null)
            redoDeque = ArrayDeque<Action>()
    }

    fun addVertex(x: Float, y: Float) {
        val action = AddVertex(x, y, ++idUniqueVertex)
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()
    }

    fun changeElement(value: String): Boolean {
        if (selected == null)
            return false

        val focused = selected
        if (focused is Edge) {

            try {
                val weight = Integer.parseInt(value)
                val action = ChangeEdgeWeight(weight, focused)
                action.execute()
                undoDeque.push(action)
                redoDeque.clear()
            } catch(e: NumberFormatException) {
                return false
            }
            return true
        } else if (focused is Vertex) {
            val action = ChangeVertexName(value, focused)
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
            return true
        }
        return false
    }

    fun deSelect() {
        selected?.unfocus()
        selected = null
    }

    fun isSelectedSmth(): Boolean = if (selected != null && (selected is Edge || selected is Vertex)) true else false

    fun isVertex(x: Float, y: Float): Boolean {
        return if (getVertexByXY(x, y) != null) true else false
    }

    fun selectVertex(x: Float, y: Float): Boolean {
        selected?.unfocus()

        selected = getVertexByXY(x, y)

        if (selected != null) {
            selected!!.focus()
            return true
        } else return false
    }

    fun deleteSelected() {
        if (selected is Vertex) {
            val action = RemoveVertex(selected as Vertex)
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
            selected = null
        } else if (selected is Edge) {
            val action = RemoveEdge(selected as Edge)
            (selected as Edge).unfocus()
            selected = null
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
        }

    }

    private fun getEdgeByValues(from: Int, to: Int, weight: Int): Edge? {
        graphView.edges.forEach { edge ->
            if (edge.from.id == from && edge.to.id == to && edge.weight == weight)
                return edge
        }
        return null
    }

    private fun getVertex(vertex: Vertex) {
        selected?.unfocus()
        selected = vertex
        selected?.focus()
    }

    fun select(x: Float, y: Float): Boolean {
        if (selected != null)
            selected!!.unfocus()

        selected = getVertexByXY(x, y)
        if (selected == null)
            selected = getEdgeByXY(x, y)

        if (selected != null) {
            selected!!.focus()
            return true
        }

        return false
    }

    fun setDirection(isDirected: Boolean) {
        graphModel.isDirected = isDirected
        graphView.directedEdges = isDirected
    }

    private fun getEdgeByXY(x: Float, y: Float): Edge? {
        graphView.edges.forEach { edge ->
            // (x - x1) / (x2 - x1) = (y - y1) / (y2 - y1) - equals of direct (edge)

            // expressing x
            // x - x1 = (y - y1) * (x2 - x1) / (y2 - y1)
            // x = (y - y1) * (x2 - x1) / (y2 - y1) + x1

            // expressing y
            // y - y1 = (x - x1) * (y2 - y1) / (x2 - x1)
            // y = (x - x1) * (y2 - y1) / (x2 - x1) + y1
            val cx = ((y - edge.from.y) * (edge.to.x - edge.from.x)) / (edge.to.y - edge.from.y) + edge.from.x
            val cy = ((x - edge.from.x) * (edge.to.y - edge.from.y)) / (edge.to.x - edge.from.x) + edge.from.y
            // (x - x1) * (y2 - y1) = (y - y1) * (x2 - x1)

            val pairX = if (edge.from.x < edge.to.x) edge.from.x to edge.to.x else edge.to.x to edge.from.x
            val pairY = if (edge.from.y < edge.to.y) edge.from.y to edge.to.y else edge.to.y to edge.from.y
            if ((Math.abs(cx - x) < GraphView.EDGE_SELECTIN_WIDTH &&
                    Math.abs(cy - y) < GraphView.EDGE_SELECTIN_WIDTH) &&
                    x > pairX.first && x < pairX.second &&
                    y > pairY.first && y < pairY.second)
                return edge
        }

        return null
    }

    private fun getVertexByXY(x: Float, y: Float): Vertex? {
        graphView.vertexes.forEach { vertex ->
            val dx = Math.abs(vertex.x - x)
            val dy = Math.abs(vertex.y - y)
            if ((dx < graphView.vertexesRadius) && ((dy < graphView.vertexesRadius) || (dy < graphView.vertexesRadius + GraphDrawer.VERTEX_TEXT_POSITION_OFFSET)))
                return vertex
        }
        return null
    }

    fun isVertexSelected(): Boolean = selected != null && selected!! is Vertex

    private fun getVertexByNumber(id: Int): Vertex? {
        graphView.vertexes.forEach { v ->
            if (v.id == id)
                return v
        }
        return null
    }

    fun moveVertex(dx: Float, dy: Float) {
        if (selected is Vertex) {
            val vertex = selected as Vertex
            if (undoDeque.peek() != null &&
                    undoDeque.peek() is MoveVertex &&
                    (undoDeque.peek() as MoveVertex).vertex == vertex) {//if move the same vertex
                (undoDeque.peek() as MoveVertex).add(dx, dy)
            } else {
                val action = MoveVertex(dx, dy, vertex)
                action.execute()
                undoDeque.push(action)
            }
        }
    }

    fun addEdge(x: Float, y: Float): Boolean {
        val vertexFrom = selected as Vertex
        val vertexTo = getVertexByXY(x, y) ?: return false

        if (vertexFrom == vertexTo)
            return false

        val action = AddEdge(vertexFrom, vertexTo)
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()

        return true
    }

    fun delete(x: Float, y: Float) {
        val vertex = getVertexByXY(x, y)
        if (vertex != null) {
            val action = RemoveVertex(vertex)
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
        }
        val edge = getEdgeByXY(x, y)
        if (edge != null) {
            val action = RemoveEdge(edge)
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
        }
    }

    fun undo(): Boolean {
        if (undoDeque.isEmpty())
            return false
        val action = undoDeque.pop()
        action.undo()
        redoDeque.push(action)
        return true
    }

    fun redo(): Boolean {
        if (redoDeque.isEmpty())
            return false
        val action = redoDeque.pop()
        action.execute()
        undoDeque.push(action)
        return true
    }

    //============================================Algorithms=======================================

    fun runDijkstra(x: Float, y: Float): Int {
        if (selected !is Vertex)
            return -1

        val secondVertex = getVertexByXY(x, y)
        val selectedVertex = selected as Vertex

        if (selectedVertex == secondVertex)
            return 0

        if (secondVertex != null) {
            val action = RunDijkstra(selectedVertex, secondVertex)
            action.execute()
            undoDeque.push(action)
            redoDeque.clear()
            if (action.result == null) return -1

            return action.result!!.sumBy(Edge::weight)
        }

        return -1
    }

    fun clearAlgorithms() {
        val action = ClearAlgorithms()
        action.execute()
        undoDeque.add(action)
        redoDeque.clear()
    }

    fun runDFS() {
        if (selected != null && selected !is Vertex)
            return
        val action = DFS(selected as Vertex)
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()
    }

    fun runKruskal(): Int {
        val action = RunKruskal()
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()
        return action.result!!.sumBy(Edge::weight)
    }

    fun runPrim(): Int {
        val action = RunPrim()
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()
        return action.result!!.sumBy(Edge::weight)
    }

    fun runEulerian(): Int {
        if (!graphModel.hasEulerianPath(graphModel))
            return -1

        val action = RunEulerianPath()
        action.execute()
        undoDeque.push(action)
        redoDeque.clear()

        return action.result!!.sumBy(Edge::weight)
    }

    //============================================ ACTIONS=========================================

    interface Action {
        fun execute()
        fun undo()
    }

    private inner class AddVertex(x: Float, y: Float, val vertexId: Int) : Action {

        var undoOfEdges: MutableList<Action>? = null
        val vertex: Vertex = Vertex(x, y, vertexId)

        override fun execute() {
            graphModel.addVertex(vertexId)
            graphView.addVertex(vertex)

            undoOfEdges?.forEach { action -> action.undo() }
        }

        override fun undo() {
            undoOfEdges = mutableListOf<Action>()

            graphView.gEdges().filter { it.from == vertex || it.to == vertex }
                    .forEach { undoOfEdges!!.add(RemoveEdge(it)) }

            undoOfEdges!!.forEach { action -> action.execute() }
            vertex.unfocus()
            graphModel.removeVertex(vertexId)
            graphView.removeVertex(vertex)
        }

    }

    private inner class AddEdge(val from: Vertex, val to: Vertex) : Action {

        val edge = Edge(from, to)

        override fun execute() {
            graphModel.addEdge(from.id, to.id)
            graphView.addEdge(edge)
        }

        override fun undo() {
            graphModel.removeEdge(from.id, to.id)
            graphView.removeEdge(edge)
        }

    }

    private inner class MoveVertex(var dx: Float, var dy: Float, val vertex: Vertex) : Action {

        override fun execute() {
            vertex.move(dx, dy)
        }

        override fun undo() {
            vertex.move(-dx, -dy)
        }

        fun add(dx: Float, dy: Float) {
            this.dx += dx
            this.dy += dy
            vertex.move(dx, dy)
        }

    }

    private inner class RemoveVertex(val vertex: Vertex) : Action {
        private var removedEdges: MutableList<Action> = mutableListOf()

        override fun execute() {
            removedEdges = mutableListOf()
            graphView.gEdges()
                    .filter { it.from == vertex || it.to == vertex }
                    .forEach { removedEdges.add(RemoveEdge(it)) }

            removedEdges.forEach { action -> action.execute() }

            selected = null
            vertex.unfocus()
            graphModel.removeVertex(vertex.id)
            graphView.removeVertex(vertex)
        }

        override fun undo() {
            graphModel.addVertex(vertex.id)
            graphView.addVertex(vertex)
/*            if (selected != null)
                selected!!.unfocus()
            selected = vertex
            selected?.focus()*/
            removedEdges.forEach { action -> action.undo() }
        }

    }

    private inner class RemoveEdge(val edge: Edge) : Action {
        override fun execute() {
            graphModel.removeEdge(edge.from.id, edge.to.id)
            graphView.removeEdge(edge)
        }

        override fun undo() {
            graphModel.addEdge(edge.from.id, edge.to.id)
            graphView.addEdge(edge)
        }
    }

    private inner class ChangeEdgeWeight(val newWeight: Int, val edge: Edge) : Action {

        val oldWeight = edge.weight

        override fun execute() {
            graphModel.changeEdgeWeight(edge.from.id, edge.to.id, newWeight)
            edge.weight = newWeight
        }

        override fun undo() {
            graphModel.changeEdgeWeight(edge.from.id, edge.to.id, oldWeight)
            edge.weight = oldWeight
        }

    }

    private inner class ChangeVertexName(val newName: String, val vertex: Vertex) : Action {

        val oldName = vertex.text

        override fun execute() {
            vertex.text = newName
        }

        override fun undo() {
            vertex.text = oldName
        }

    }

    private inner class RunDijkstra(val from: Vertex, val to: Vertex) : Action {

        var result: MutableList<Edge>? = null

        override fun execute() {
            val algoResult = Algorithms.dijkstraShortestpath(graphModel, from.id, to.id) ?: return

            if (result == null)
                result = mutableListOf()
            for (i in 0..(algoResult.size - 2)) {
                graphView.edges.forEach { edge ->
                    if (edge.from.id == algoResult[i]
                            && edge.to.id == algoResult[i + 1])
                        result!!.add(edge)
                    if (!graphModel.isDirected &&
                            (edge.to.id == algoResult[i] &&
                                    edge.from.id == algoResult[i + 1]))
                        result!!.add(edge)
                }
            }

            graphView.highLights = result as MutableList<Edge>
        }

        override fun undo() {
            graphView.highLights = null
        }
    }

    private inner class ClearAlgorithms() : Action {

        private var highlights: MutableList<Edge>? = null

        override fun execute() {
            if (graphView.highLights != null) {
                highlights = graphView.highLights!!
                graphView.highLights = null
            }
        }

        override fun undo() {
            if (highlights != null)
                graphView.highLights = highlights
        }
    }

    private inner class DFS(val vertex: Vertex) : Action {

        var result: MutableList<Edge>? = null

        override fun execute() {
            if (result == null)
                result = mutableListOf()

            val res: List<Pair<Int, Int>> = Algorithms.DFS(graphModel, vertex.id)//key- prev, value - next
            res.forEach { pair ->
                val edge = Edge(getVertexByNumber(pair.first)!!, getVertexByNumber(pair.second)!!)
                if (edge != null)
                    result?.add(edge)
            }

            graphView.highLights = result
        }

        override fun undo() {
            graphView.highLights = null
        }
    }

    private inner class RunKruskal() : Action {
        var result: MutableList<Edge>? = null

        override fun execute() {
            if (result == null)
                result = mutableListOf()

            val res: Map<Int, Int> = Algorithms.Kruskal(graphModel)
            res.entries.forEach { entry ->
                graphView.edges.forEach { edge ->
                    if (entry.key == edge.to.id &&
                            entry.value == edge.from.id)
                        result!!.add(edge)
                    if (!graphView.directedEdges &&
                            (entry.key == edge.from.id && entry.value == edge.to.id))
                        result!!.add(edge)
                }
            }
            graphView.highLights = result
        }

        override fun undo() {
            graphView.highLights = null
        }
    }

    private inner class RunPrim() : Action {

        var result: MutableList<Edge>? = null

        override fun execute() {
            if (result == null)
                result = mutableListOf()

            val res: Map<Int, Int> = Algorithms.Prim(graphModel)
            res.entries.forEach { entry ->
                graphView.edges.forEach { edge ->
                    if (entry.key == edge.to.id &&
                            entry.value == edge.from.id)
                        result!!.add(edge)
                    if (!graphView.directedEdges &&
                            (entry.key == edge.from.id && entry.value == edge.to.id))
                        result!!.add(edge)
                }
            }
            graphView.highLights = result

        }

        override fun undo() {
            graphView.highLights = null
        }
    }

    private inner class RunEulerianPath() : Action {

        var result: MutableList<Edge>? = null

        override fun execute() {
            if (result == null)
                result = mutableListOf()

            val res = Algorithms.EulerianPath(graphModel)
            res.forEach {
                graphView.edges.forEach { edge ->
                    if (edge.from.id == it.first
                            && edge.to.id == it.second)
                        result!!.add(edge)
                    if (!graphModel.isDirected &&
                            (edge.to.id == it.first &&
                                    edge.from.id == it.second))
                        result!!.add(edge)
                }
            }
/*
            for (i in 0..(res.size - 2) ) {
                graphView.edges.forEach { edge ->
                    if (edge.from.id == res[i]
                            && edge.to.id == res[i + 1])
                        result!!.add(edge)
                    if (!graphModel.isDirected &&
                            (edge.to.id == res[i] &&
                                    edge.from.id == res[i + 1]))
                        result!!.add(edge)
                }
            }
*/

            graphView.highLights = result
        }

        override fun undo() {
            graphView.highLights = null
        }
    }

}
