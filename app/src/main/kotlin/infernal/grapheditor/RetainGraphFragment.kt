package infernal.grapheditor

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.Snackbar
import infernal.grapheditor.GraphController
import infernal.grapheditor.GraphDrawer

/**
 * Created by xxinf on 26-Nov-16.
 */
class RetainGraphFragment : Fragment() {
    var graphController: GraphController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

}