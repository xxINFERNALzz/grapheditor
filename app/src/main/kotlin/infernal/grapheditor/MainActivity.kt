package infernal.grapheditor

import android.content.Context
import android.os.*
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.EditorInfo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream


class MainActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        View.OnTouchListener {

    private enum class Mode { NORMAL, ADD_VERTEX, ADD_EDGE, CHANGE_NAME, DELETE,
        DIJKSTRA, DFS, EULERIAN_PATH
    }

    private var mode = Mode.NORMAL
    private val TAG: String = "Graph.MainActivity"
    private var isShowHelp: Boolean = true
    private var graphController: GraphController? = null
    private var graphDrawer: GraphDrawer? = null

    private var snackbar: Snackbar? = null
    private var imm: InputMethodManager? = null
    private var scaleDetector: ScaleGestureDetector? = null
    private var algorithmTask: AsyncTask<Void, Void, Int>? = null
    private var retainGraphFragment: RetainGraphFragment? = null

    private var algorithmShown = false
    private var drag: Boolean = false
    private var dragX: Float = 0f
    private var dragY: Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(this.toolbar)

        val toggle = ActionBarDrawerToggle(
                this, this.drawer_layout, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        this.drawer_layout!!.setDrawerListener(toggle)
        toggle.syncState()

        this.nav_view!!.setNavigationItemSelectedListener(this)

        //my par
        retainGraphFragment = fragmentManager.findFragmentByTag("graph") as RetainGraphFragment?
        if (retainGraphFragment == null) {
            graphController = GraphController()
            retainGraphFragment = RetainGraphFragment()
            fragmentManager.beginTransaction().add(retainGraphFragment, "graph").commit()
            retainGraphFragment!!.graphController = graphController
        } else {
            graphController = retainGraphFragment!!.graphController
        }

        graphDrawer = GraphDrawer(this, graphController!!.graphView)
        graphDrawer!!.setOnTouchListener(this)

        this.relative_layout_for_graph_drawer!!.addView(graphDrawer)

        imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.editText!!.setOnEditorActionListener { textView, actionId, keyEvent ->
            var handled = false
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (!graphController!!.changeElement(textView.text.toString()))
                    showSnackbarMessage("Edge weight must be integer")
                hideTextInput()
                handled = true
                graphDrawer!!.invalidate()
            } else if (actionId == KeyEvent.KEYCODE_BACK) {
                hideTextInput()
                mode = Mode.NORMAL
                handled = true
            }

            return@setOnEditorActionListener handled
        }

        scaleDetector = ScaleGestureDetector(this, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val scaleFactor = detector.scaleFactor
/*                graphDrawer!!.setScale(graphDrawer!!._scaleX * scaleFactor,
                        graphDrawer!!._scaleY * scaleFactor)*/

                graphController!!.graphView.vertexesRadius *= scaleFactor
                //Math.max(GraphDrawer.MIN_SCALE_LIMIT, Math.min(scaleFactor, GraphDrawer.MAX_SCALE_LIMIT))
                graphDrawer!!.scaling(scaleFactor)

                //graphDrawer!!.setBackgroundColor(Color.RED)

                graphDrawer!!.invalidate()
                return true
            }
        })
    }

    override fun onDestroy() {
        if (algorithmTask?.status != AsyncTask.Status.FINISHED)
            algorithmTask?.cancel(true)
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putBoolean("isShowHelp", isShowHelp)
        outState?.putBoolean("algoritmShown", algorithmShown)
        graphController?.clearAlgorithms()
        retainGraphFragment?.graphController = graphController
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        isShowHelp = savedInstanceState?.getBoolean("isShowHelp")!!
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout?
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            if (!hideTextInput())
                super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_add_vertex) {
            if (algorithmShown)
                clearAlgorithms()
            mode = Mode.ADD_VERTEX
            if (isShowHelp)
                showSnackbarMessage(resources.getString(R.string.help_add_vector))
            return true
        } else if (id == R.id.action_add_edge) {
            if (algorithmShown)
                clearAlgorithms()
            mode = Mode.ADD_EDGE
            graphController!!.deSelect()
            graphDrawer!!.invalidate()
            if (isShowHelp)
                showSnackbarMessage(resources.getString(R.string.help_add_edge))
            return true
        } else if (id == R.id.action_add_name) {
            if (algorithmShown)
                clearAlgorithms()
            mode = Mode.CHANGE_NAME
            if (graphController!!.isSelectedSmth()) {
//                graphController!!.deSelect()
                addNameTouch(null)
                graphDrawer!!.invalidate()
            }
            if (isShowHelp)
                showSnackbarMessage(resources.getString(R.string.help_change_name))
            return true
        } else if (id == R.id.action_delete) {
            if (algorithmShown)
                clearAlgorithms()
            if (graphController!!.isSelectedSmth()) {
                graphController!!.deleteSelected()
                graphDrawer!!.invalidate()
            } else {
                mode = Mode.DELETE
                graphController!!.deSelect()
                if (isShowHelp)
                    showSnackbarMessage(resources.getString(R.string.help_delete))
            }
            return true
        } else if (id == R.id.action_undo) {
            graphController!!.undo()
            graphDrawer!!.invalidate()
            return true
        } else if (id == R.id.action_redo) {
            graphController!!.redo()
            graphDrawer!!.invalidate()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        when (id) {
            R.id.save_to_file -> {
                saveToFile()
            }
            R.id.load_from_file -> {
                loadFromFile()
            }
            R.id.switch_direction -> {
                if (graphController!!.isDirected())
                    graphController!!.setDirection(false)
                else graphController!!.setDirection(true)
                graphDrawer!!.invalidate()
            }
            R.id.help -> {
                if (isShowHelp)
                    isShowHelp = false
                else isShowHelp = true
            }
            R.id.Dijkstra -> {
                if (algorithmShown)
                    clearAlgorithms()
                graphController!!.deSelect()
                mode = Mode.DIJKSTRA
                if (isShowHelp)
                    showSnackbarMessage("Tap on outbound vertex")
            }
            R.id.DFS -> {
                if (algorithmShown)
                    clearAlgorithms()
                graphController!!.deSelect()
                mode = Mode.DFS
                if (isShowHelp)
                    showSnackbarMessage("Tap on outbound vertex")
            }
            R.id.Kruskal -> {
                if (algorithmShown)
                    clearAlgorithms()
                graphController!!.deSelect()
                if (!graphController!!.isDirected()) {
                    algorithmShown = true
                    algorithmTask = object : AsyncTask<Void, Void, Int>() {
                        override fun doInBackground(vararg params: Void?): Int {
                            return graphController!!.runKruskal()
                        }

                        override fun onPostExecute(result: Int?) {
                            graphDrawer!!.invalidate()
                            showSnackbarAlgorithmResult(resources.getString(R.string.algorithm_kruskal_done, result))
                        }
                    }.execute()
                } else
                    showSnackbarMessage("Kruskal\'s algorithm working for undirected graphs only")
            }
            R.id.Prim -> {
                if (algorithmShown)
                    clearAlgorithms()
                graphController!!.deSelect()
                if (!graphController!!.isDirected()) {
                    algorithmShown = true
                    algorithmTask = object : AsyncTask<Void, Void, Int>() {
                        override fun doInBackground(vararg params: Void?): Int {
                            return graphController!!.runPrim()
                        }

                        override fun onPostExecute(result: Int?) {
                            graphDrawer!!.invalidate()
                            showSnackbarAlgorithmResult(resources.getString(R.string.algorithm_kruskal_done, result))
                        }
                    }.execute()
                } else
                    showSnackbarMessage("Prim\'s algorithm working for undirected graphs only")
            }
            R.id.Eulerian_path -> {
                if (algorithmShown)
                    clearAlgorithms()
                graphController!!.deSelect()
                if (!graphController!!.isDirected()) {
                    algorithmShown = true

                    algorithmTask = object : AsyncTask<Void, Void, Int>() {
                        override fun doInBackground(vararg params: Void?): Int {
                            return graphController!!.runEulerian()
                        }

                        override fun onPostExecute(result: Int?) {
                            if (result != -1)
                                showSnackbarAlgorithmResult(resources.getString(R.string.algorithm_eulerian_path_done, result))

                            graphDrawer!!.invalidate()
                        }
                    }.execute()
                } else
                    showSnackbarMessage("Eulerian path algorithm working for undirected graphs only")
            }
        }

        this.drawer_layout!!.closeDrawer(GravityCompat.START)
        return false
    }

    private fun saveToFile() {
        var fos: FileOutputStream? = null
        var os: ObjectOutputStream? = null
        try {
            fos = openFileOutput("graph", Context.MODE_PRIVATE)
            os = ObjectOutputStream(fos)
            os.writeObject(graphController)
            showSnackbarMessage("Graph saved")
        } catch(e: Exception) {
            showSnackbarMessage("Oops! Something happened, saving failed!")
            e.printStackTrace()
        } finally {
            fos?.close()
            os?.close()
        }
    }

    private fun loadFromFile() {
/*        val intent = Intent(Intent.ACTION_GET_CONTENT)
        val uri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/myFolder/")
        intent.setDataAndType(uri, "text/csv")
        startActivity(Intent.createChooser(intent, "Open folder"))*/

        mode = Mode.NORMAL
        var fis: FileInputStream? = null
        var ins: ObjectInputStream? = null

        try {
            fis = openFileInput("graph")
            ins = ObjectInputStream(fis)
            graphController = ins.readObject() as GraphController
            graphController?.reInit()
            graphDrawer = GraphDrawer(this, graphController!!.graphView)
            graphDrawer!!.setOnTouchListener(this)

            this.relative_layout_for_graph_drawer!!.removeAllViewsInLayout()
            this.relative_layout_for_graph_drawer!!.addView(graphDrawer)
            graphDrawer!!.invalidate()
            showSnackbarMessage("Graph loaded!")
        } catch(e: Exception) {
            showSnackbarMessage("Oops! Something happened, loading failed!")
            e.printStackTrace()
        } finally {
            fis?.close()
            ins?.close()
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        Log.v(TAG, mode.name)//check mode
        return when (mode) {
            Mode.ADD_VERTEX -> {
                mode = Mode.NORMAL
                addVertexTouch(event)
            }
            Mode.ADD_EDGE -> addEdgeTouch(event)
            Mode.CHANGE_NAME -> addNameTouch(event)
            Mode.DELETE -> deleteTouch(event)
            Mode.DIJKSTRA -> dijkstraModeTouch(event)
            Mode.DFS -> dfsModeTouch(event)
            else -> {
                if (event!!.pointerCount == 1) // if touch
                    normalTouch(event)
                else scaleDetector!!.onTouchEvent(event)
            }
        }
    }

    private fun dfsModeTouch(event: MotionEvent?): Boolean {
        val x = event!!.x - graphDrawer!!.initialX
        val y = event.y - graphDrawer!!.initialY

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (graphController!!.isVertex(x, y)) {
                    graphController!!.selectVertex(x, y)

                    algorithmTask = object : AsyncTask<Void, Void, Int>() {
                        override fun doInBackground(vararg params: Void?): Int {
                            graphController!!.runDFS()
                            return 0
                        }

                        override fun onPostExecute(result: Int?) {
                            graphDrawer!!.invalidate()
                        }
                    }.execute()
                    algorithmShown = true
                    showSnackbarAlgorithmResult("DFS done")
                }
                drag = true
                dragX = event.x
                dragY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                if (drag) {
                    graphDrawer!!.move((event.x - dragX)/* / graphDrawer!!._scaleX*/,
                            (event.y - dragY)/* / graphDrawer!!._scaleY*/)
                    dragX = event.x
                    dragY = event.y

                    graphDrawer!!.invalidate()
                }
            }
            MotionEvent.ACTION_UP -> drag = false
        }
        return true
    }

    private fun dijkstraModeTouch(event: MotionEvent?): Boolean {
        val x = event!!.x - graphDrawer!!.initialX// add scale dependency
        val y = event.y - graphDrawer!!.initialY
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (graphController!!.isVertexSelected()) {//maybe add check if node on the same place
                    if (graphController!!.isVertex(x, y)) {
                        if (isShowHelp) showSnackbarMessage("Dijkstra performing...")

                        algorithmTask = object : AsyncTask<Void, Void, Int>() {

                            override fun doInBackground(vararg params: Void?): Int {
                                return graphController!!.runDijkstra(x, y)
                            }

                            override fun onPostExecute(result: Int?) {
                                val resultStr = if (result == -1)
                                    resources.getString(R.string.algorithm_dijkstra_no_path)
                                else {
                                    algorithmShown = true
                                    resources.getString(R.string.algorithm_path, result)
                                }
                                showSnackbarAlgorithmResult(resultStr)
                                graphDrawer!!.invalidate()
                            }
                        }.execute()
                    }
                } else {
                    if (graphController!!.selectVertex(x, y)) {
                        if (isShowHelp) showSnackbarMessage("Tap on inbound vertex")
                        graphDrawer!!.invalidate()
                    }
                }
                drag = true
                dragX = event.x
                dragY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                if (drag) {
                    graphDrawer!!.move((event.x - dragX)/* / graphDrawer!!._scaleX*/,
                            (event.y - dragY)/* / graphDrawer!!._scaleY*/)
                    dragX = event.x
                    dragY = event.y

                    graphDrawer!!.invalidate()
                }
            }
            MotionEvent.ACTION_UP -> drag = false
        }
        return true
    }

    private fun deleteTouch(event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_POINTER_DOWN -> {
                val x = event.x/* / graphDrawer!!._scaleX*/ - graphDrawer!!.initialX
                val y = event.y/* / graphDrawer!!._scaleY*/ - graphDrawer!!.initialY

                graphController!!.delete(x, y)
                mode = Mode.NORMAL
                graphDrawer!!.invalidate()
                return true
            }
        }
        return false
    }

    private fun addNameTouch(event: MotionEvent?): Boolean {
        if (event != null) {
            if (normalTouch(event) && graphController!!.isSelectedSmth()) {
                editText!!.visibility = View.VISIBLE
                imm!!.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
                mode = Mode.NORMAL
                return true
            }
        } else {
            editText!!.visibility = View.VISIBLE
            imm!!.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
            mode = Mode.NORMAL
            return true
        }

        return true
    }

    private fun addVertexTouch(event: MotionEvent?): Boolean {
        val x = event!!.x/* / graphDrawer!!._scaleX*/ - graphDrawer!!.initialX
        val y = event.y/* / graphDrawer!!._scaleY*/ - graphDrawer!!.initialY

        graphController!!.addVertex(x, y)
        graphDrawer!!.invalidate()

        return true
    }

    private fun addEdgeTouch(event: MotionEvent?): Boolean {
        val x = event!!.x/* / graphDrawer!!._scaleX*/ - graphDrawer!!.initialX
        val y = event.y/* / graphDrawer!!._scaleY*/ - graphDrawer!!.initialY

        return if (graphController!!.isVertexSelected()) {
            if (graphController!!.addEdge(x, y)) {
                graphDrawer!!.invalidate()
                mode = Mode.NORMAL
                true
            } else false
        } else normalTouch(event)
    }

    private fun normalTouch(event: MotionEvent?): Boolean {
        val x = event!!.x/* / graphDrawer!!._scaleX*/ - graphDrawer!!.initialX
        val y = event.y/* / graphDrawer!!._scaleY*/ - graphDrawer!!.initialY

        if (imm!!.isActive && mode == Mode.CHANGE_NAME) {
            imm!!.hideSoftInputFromWindow(editText!!.windowToken, 0)
            editText!!.visibility = View.INVISIBLE
        }
/*        if (algorithmShown)
            clearAlgorithms()*/

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                graphController!!.select(x, y)
                drag = true
                dragX = event.x
                dragY = event.y
                graphDrawer!!.invalidate()
            }
            MotionEvent.ACTION_MOVE -> {
                if (drag) {
                    if (graphController!!.isVertexSelected()) {
                        graphController!!.moveVertex(
                                (event.x - dragX)/* / graphDrawer!!._scaleX*/,
                                (event.y - dragY)/* / graphDrawer!!._scaleY*/)
                        dragX = event.x
                        dragY = event.y
                    } else {
                        graphDrawer!!.move((event.x - dragX)/* / graphDrawer!!._scaleX*/,
                                (event.y - dragY)/* / graphDrawer!!._scaleY*/)
                        dragX = event.x
                        dragY = event.y
                    }
                    graphDrawer!!.invalidate()
                }
            }
            MotionEvent.ACTION_UP -> drag = false
        }
        return true
    }

    private fun showSnackbarMessage(message: String) {
        snackbar = Snackbar.make(this.relative_layout_for_graph_drawer!! as View, message, Snackbar.LENGTH_LONG)
        snackbar!!.show()
    }

    private fun showSnackbarAlgorithmResult(message: String) {
        snackbar = Snackbar.make(this.relative_layout_for_graph_drawer!! as View,
                message,
                Snackbar.LENGTH_INDEFINITE)
                .setAction("Clear Algorithm", { clearAlgorithms() })
        snackbar?.show()
    }

    private fun hideTextInput(): Boolean {
        if (imm!!.isActive) {
            imm!!.hideSoftInputFromWindow(editText!!.windowToken, 0)
            editText!!.visibility = View.INVISIBLE
            return true
        }
        return false
    }

    private fun clearAlgorithms() {
        snackbar?.dismiss()
        algorithmShown = false
        mode = Mode.NORMAL
        graphController!!.clearAlgorithms()
        graphDrawer!!.invalidate()
    }
}
