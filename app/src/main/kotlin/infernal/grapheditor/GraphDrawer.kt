package infernal.grapheditor

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.view.View
import infernal.grapheditor.view.GraphView

/**
 * Created by xxinf on 13-Nov-16.
 */
class GraphDrawer(context: Context,
                  private var graphView: GraphView) : View(context) {

    private val vertexPaint: Paint
    private val edgePaint: Paint
    private val selectionPaint: Paint
    private val borderPaint: Paint
    private val namesPaint: Paint
    private val highlightsPaint: Paint

    var initialX: Float = 0f
    var initialY: Float = 0f

    /*
        var _scaleX: Float = 1f
        var _scaleY: Float = 1f
    */
    var scaling = 1f


    init {
        vertexPaint = Paint()
        vertexPaint.color = VERTEX_COLOR
        vertexPaint.textSize = TEXT_SIZE

        edgePaint = Paint()
        edgePaint.strokeWidth = EDGE_STROKE_WIDTH
        edgePaint.textSize = TEXT_SIZE

        selectionPaint = Paint()
        selectionPaint.color = SELECTION_COLOR
        selectionPaint.style = Paint.Style.STROKE
        selectionPaint.strokeWidth = SELECTION_STROKE_WIDTH
        selectionPaint.textSize = TEXT_SIZE

        borderPaint = Paint()
        borderPaint.color = BORDER_COLOR
        borderPaint.style = Paint.Style.STROKE
        borderPaint.strokeWidth = BORDER_STROKE_WIDTH

        namesPaint = Paint()
        namesPaint.color = NAMES_COLOR
        namesPaint.strokeWidth = NAMES_STROKE_WIDTH
        namesPaint.textSize = TEXT_SIZE

        highlightsPaint = Paint()
        highlightsPaint.color = HIGHLIGHT_COLOR
        highlightsPaint.strokeWidth = HIGHLIGHT_STROKE_WIDTH
    }

    fun move(dx: Float, dy: Float) {
        initialX += dx
        initialY += dy
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.save()
        canvas.scale(scaleX, scaleY)


        for (edge in graphView.gEdges()) {
            val paint = if (edge.isFocused()) selectionPaint else edgePaint

            val x1 = (initialX + edge.x1())
            /** scaling*/
            val x2 = (initialX + edge.x2())
            /** scaling*/
            val y1 = (initialY + edge.y1())
            /** scaling*/
            val y2 = (initialY + edge.y2())
            /** scaling*/

            canvas.drawLine(x1, y1, x2, y2, paint)

            val weightPos = edge.weightPosition()
            canvas.drawText(edge.weight.toString(),
                    (weightPos.first + initialX)
                    /** scaling*/
                    ,
                    (weightPos.second + initialY)
                    /** scaling*/
                    ,
                    paint)

            if (graphView.directedEdges) {
                val angle = edge.angle()
                val sin = Math.sin(angle)
                val cos = Math.cos(angle)

                val x1 = (edge.from.x + (graphView.vertexesRadius * cos)).toFloat()
                val x2 = (edge.to.x - (graphView.vertexesRadius * cos)).toFloat()
                var y1 = 0f
                var y2 = 0f

                if (edge.from.y < edge.to.y) {
                    y1 = (edge.from.y + (graphView.vertexesRadius * sin)).toFloat()
                    y2 = (edge.to.y - (graphView.vertexesRadius * sin)).toFloat()
                } else {
                    y1 = (edge.from.y - (graphView.vertexesRadius * sin)).toFloat()
                    y2 = (edge.to.y + (graphView.vertexesRadius * sin)).toFloat()
                }

                var angle1 = Math.PI / 6 + angle
                var angle2 = angle - Math.PI / 6
                if (y1 < y2) {
                    angle1 *= -1
                    angle2 *= -1
                }

                val tip1X = (((x2 + initialX)
                        /** scaling*/
                        ) - EDGE_ARROW_LENGTH * Math.cos(angle1)).toFloat()
                val tip1Y = (((y2 + initialY)
                        /** scaling*/
                        ) + EDGE_ARROW_LENGTH * Math.sin(angle1)).toFloat()

                val tip2X = (((x2 + initialX)
                        /** scaling*/
                        ) - EDGE_ARROW_LENGTH * Math.cos(angle2)).toFloat()
                val tip2Y = (((y2 + initialY)
                        /** scaling*/
                        ) + EDGE_ARROW_LENGTH * Math.sin(angle2)).toFloat()

                val path = Path()
                path.moveTo((x2 + initialX)
                        /** scaling*/
                        , (y2 + initialY)
                        /** scaling*/
                )
                path.lineTo(tip1X, tip1Y)
                path.lineTo(tip2X, tip2Y)
                path.moveTo(tip1X, tip1Y)
                path.lineTo(tip2X, tip2Y)
                path.lineTo((x2 + initialX)
                        /** scaling*/
                        , (y2 + initialY)
                        /** scaling*/
                )
                canvas.drawPath(path, paint)
            }

        }

        for (vertex in graphView.gVertexes()) {
            val x = (initialX + vertex.x)
            /** scaling*/
            val y = (initialY + vertex.y)
            /** scaling*/
            canvas.drawCircle(x, y, graphView.vertexesRadius, vertexPaint)

            if (vertex.isFocused()) {
                canvas.drawCircle(x, y, graphView.vertexesRadius, selectionPaint)
                canvas.drawText(vertex.text, x, y + VERTEX_TEXT_POSITION_OFFSET * scaling, selectionPaint)
            } else {
                canvas.drawCircle(x, y, graphView.vertexesRadius, borderPaint)
                canvas.drawText(vertex.text, x, y + VERTEX_TEXT_POSITION_OFFSET * scaling, namesPaint)
            }
        }

        if (graphView.highLights != null)
            for (highlight in graphView.highLights!!) {
                val x1 = highlight.x1() + initialX
                val x2 = highlight.x2() + initialX
                val y1 = highlight.y1() + initialY
                val y2 = highlight.y2() + initialY

                canvas.drawLine(x1, y1, x2, y2, highlightsPaint)
            }

        canvas.restore()
    }

    fun setScale(scaleX: Float, scaleY: Float) {
/*        _scaleX = Math.max(MIN_SCALE_LIMIT, Math.min(scaleX, MAX_SCALE_LIMIT))
        _scaleY = Math.max(MIN_SCALE_LIMIT, Math.min(scaleY, MAX_SCALE_LIMIT))*/

        //graphView.vertexesRadius *= Math.max(GraphDrawer.MIN_SCALE_LIMIT, Math.min(_scaleX, GraphDrawer.MAX_SCALE_LIMIT))
        //fixme: fix vertex radius change, while scaling
    }

    public companion object {
        @JvmStatic val VERTEX_COLOR = Color.GREEN
        @JvmStatic val BORDER_COLOR = Color.BLACK
        @JvmStatic val SELECTION_COLOR = Color.RED
        @JvmStatic val NAMES_COLOR = Color.BLACK
        @JvmStatic val HIGHLIGHT_COLOR = Color.MAGENTA
        @JvmStatic val EDGE_STROKE_WIDTH = 4f
        @JvmStatic val SELECTION_STROKE_WIDTH = 6f
        @JvmStatic val BORDER_STROKE_WIDTH = 2f
        @JvmStatic val NAMES_STROKE_WIDTH = 14f
        @JvmStatic val HIGHLIGHT_STROKE_WIDTH = 7f
        @JvmStatic val VERTEX_RADIUS = 20f
        @JvmStatic val TEXT_SIZE = 22f
        @JvmStatic val VERTEX_TEXT_POSITION_OFFSET = 50
        @JvmStatic private val EDGE_ARROW_LENGTH = 35

        @JvmStatic val MIN_SCALE_LIMIT = 0.3f
        @JvmStatic val MAX_SCALE_LIMIT = 4f
    }

    fun scaling(scaleFactor: Float) {
        scaling *= scaleFactor
        /*
        scaleX = Math.max(0.1f, Math.min(scaling, 5.0f))
        scaleY = Math.max(0.1f, Math.min(scaling, 5.0f))*/
    }


}