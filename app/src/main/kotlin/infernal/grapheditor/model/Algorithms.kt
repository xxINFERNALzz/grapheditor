package infernal.grapheditor.model

import java.util.*

/**
 * Created by xxinf on 14-Nov-16.
 */
object Algorithms {

    @JvmStatic fun dijkstraShortestpath(graphModel: GraphModel, source: Int, destination: Int): List<Int>? {
        val openSet = TreeSet<Int>(graphModel.getVertexes())//unvisited vertexes
        val closeMap: MutableMap<Int, Int> = mutableMapOf(source to 0)//distance(weight)
        val prevVertexes: MutableMap<Int, Int> = mutableMapOf()

        while (!openSet.isEmpty()) {
            var min = -1
/*
            openSet
                    .asSequence()
                    .filter { closeMap.containsKey(it) && (min == -1 || closeMap[it]!! > closeMap[min]!!) }
                    .forEach { min = it }
*/
            for (i in openSet) {
                if (closeMap.containsKey(i) && (min == -1 || closeMap[i]!! > closeMap[min]!!))
                    min = i
            }
            if (min == -1)
                break

            val currentVertex = min

            openSet.remove(currentVertex)

            graphModel.neighbourVertexes(currentVertex).forEach { edge ->
                val neighbourVertex = if (edge.to != currentVertex) edge.to else edge.from
                val alt = closeMap[currentVertex]!! + edge.weight
                if (!closeMap.containsKey(neighbourVertex) || alt < closeMap[neighbourVertex]!!) {
                    closeMap.put(neighbourVertex, alt)
                    prevVertexes.put(neighbourVertex, currentVertex)
                }
            }
        }

        val result = mutableListOf<Int>()
        if (!prevVertexes.containsKey(destination))
            return null

        var vertex = destination
        while (vertex != source) {
            result.add(vertex)
            vertex = prevVertexes[vertex]!!
        }

        result.add(source)

        return result.reversed()
    }

    @JvmStatic fun DFS(graphModel: GraphModel, vertex: Int): List<Pair<Int, Int>> {
        val result = mutableListOf<Pair<Int, Int>>()
        val stack = ArrayDeque<Pair<Int, Int>>()
        val discovered = mutableSetOf<Int>()

        stack.push(Pair(vertex, vertex))
        while (!stack.isEmpty()) {
            val pair = stack.pop()
            if (!discovered.contains(pair.second)) {
                discovered.add(pair.second)
                result.add(pair)

                val vertex = pair.second
                graphModel.edges(vertex).forEach { edge ->
                    val adjacentVertex = if (vertex != edge.to) edge.to else edge.from
                    if (!discovered.contains(adjacentVertex))
                        stack.push(Pair(vertex, adjacentVertex))
                }
            }
        }
        return result
    }

    @JvmStatic fun Kruskal(graphModel: GraphModel): Map<Int, Int> {
        if (graphModel.isDirected)
            throw UnsupportedOperationException("Kruskal\'s algorithm working for undirected graphs only")
        val result = mutableMapOf<Int, Int>()
        val edges = graphModel.edges().asSequence().toMutableList()

        edges.sortBy { e -> e.weight }

        val set = DisjoinSet<Int>()
        graphModel.getVertexes().forEach { set.makeSet(it) }

        edges.forEach { edge ->
            if (set.find(edge.to) != set.find(edge.from)) {
                if (!result.containsKey(edge.to))
                    result.put(edge.to, edge.from)
                else
                    result.put(edge.from, edge.to)

                set.union(edge.to, edge.from)
            }
        }

        return result
    }

    class DisjoinSet<T> {

        private inner class Node(val id: T) {
            var parent = this
            var rank = 0
        }

        private val map = mutableMapOf<T, Node>()

        public fun makeSet(x: T) {
            if (!map.containsKey(x))
                map.put(x, Node(x))
        }


        fun union(x: T, y: T) {
            val xRoot = find(map[x]!!)
            val yRoot = find(map[y]!!)
            if (xRoot === yRoot)
                return

            if (xRoot.rank < yRoot.rank)
                xRoot.parent = yRoot
            else if (xRoot.rank > yRoot.rank)
                yRoot.parent = xRoot
            else {
                yRoot.parent = xRoot
                xRoot.rank++
            }
        }

        fun find(x: T): T = find(map[x]!!).id


        private fun find(x: Node): Node {
            if (x.parent !== x)
                x.parent = find(x.parent)
            return x.parent
        }
    }

    @JvmStatic fun Prim(graphModel: GraphModel): Map<Int, Int> {
        if (graphModel.isDirected)
            throw UnsupportedOperationException("Prim's algorithm works only for undirected graphs")
        val tree = mutableMapOf<Int, Int>()
        val edges = mutableListOf<GraphModel.Edge>()
        val labeled = HashSet<Int>()
        val vertexIt = graphModel.getVertexes().iterator()

        while (vertexIt.hasNext()) {
            val vertex = vertexIt.next()
            if (labeled.contains(vertex))
                continue
            labeled.add(vertex)
            edges.addAll(graphModel.neighbourVertexes(vertex))
            while (!edges.isEmpty()) {

                edges.sortBy { e -> e.weight }
                val it = edges.iterator()
                while (it.hasNext()) {
                    val edge = it.next()
                    if (!labeled.contains(edge.to)) {
                        labeled.add(edge.to)
                        edges.addAll(graphModel.neighbourEdges(edge.to))
                        tree.put(edge.to, edge.from)
                        break
                    }
                    if (!labeled.contains(edge.from)) {
                        labeled.add(edge.from)
                        edges.addAll(graphModel.neighbourEdges(edge.from))
                        tree.put(edge.from, edge.to)
                        break
                    }
                    it.remove()
                }
            }
        }
        return tree
    }

    @JvmStatic fun EulerianPath(graphModel: GraphModel): List<Pair<Int, Int>> {
        if (graphModel.isDirected)
            throw UnsupportedOperationException("Eulerian's algorithm works only for undirected graphs")

        val vertexes = graphModel.getVertexes().asSequence().toMutableList()
        val result = mutableListOf<Pair<Int, Int>>()
        var edges = graphModel.edges().asSequence().toMutableList()

        var vertNumCounter = 0
        do {
            result.clear()
            //Collections.copy(edges, graphModel.edges().asSequence().toMutableList())//copy edges

            val stack = ArrayDeque<Pair<Int, Int>>()
            val vertexStack = ArrayDeque<Int>()
            vertexStack.push(vertexes[vertNumCounter])
            stack.push(vertexes[vertNumCounter] to vertexes[vertNumCounter])
            while (!vertexStack.isEmpty()) {
                val vertex = vertexStack.peek()
                val pair = stack.peek()
                if (edges.filter { it.from == vertex || it.to == vertex }.count() == 0) {
                    if (pair != null && pair != vertexes[vertNumCounter] to vertexes[vertNumCounter])
                        result.add(pair)
                    stack.pop()
                    vertexStack.pop()
                } else {
                    val edge = edges.filter { it.from == vertex || it.to == vertex }.firstOrNull()
                    if (edge != null) {
                        edges.remove(edge)
                        vertexStack.push(if (edge.to == vertex) edge.from else edge.to)
                        stack.push(edge.from to edge.to)
                    }
                }
            }
            edges = graphModel.edges().asSequence().toMutableList()
            vertNumCounter++
        } while (vertNumCounter < vertexes.size || result.size != edges.size)
        return result
    }
}