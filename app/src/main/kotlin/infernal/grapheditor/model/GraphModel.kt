package infernal.grapheditor.model

import java.io.Serializable

/**
 * Created by xxinf on 14-Nov-16.
 */
class GraphModel(var isDirected: Boolean = false) : Serializable {
    data class Edge(val from: Int, val to: Int, var weight: Int) : Serializable

    private val adjOut: MutableMap<Int, MutableList<Edge>> = mutableMapOf()
    private val adjIn: MutableMap<Int, MutableList<Edge>> = mutableMapOf()

    companion object {
        @JvmStatic val DEFAULT_EDGE_WEIGHT = 1
    }

    fun addVertex(vertexId: Int): Boolean {
        if (adjOut.contains(vertexId))
            return false

        adjOut.put(vertexId, mutableListOf())
        adjIn.put(vertexId, mutableListOf())
        return true
    }

    fun addEdge(fromId: Int, toId: Int) {
        val edge = Edge(fromId, toId, DEFAULT_EDGE_WEIGHT)

        if (adjOut.containsKey(fromId)) {
            if (adjOut[fromId] == null)
                adjOut[fromId] = mutableListOf(edge)
            else adjOut[fromId]!!.add(edge)
        } else return

        if (adjIn.containsKey(toId)) {
            if (adjIn[toId] == null)
                adjIn[toId] = mutableListOf(edge)
            else adjIn[toId]!!.add(edge)
        }
    }

    fun removeVertex(vertexId: Int) {
        if (adjOut.containsKey(vertexId)) {
            adjOut[vertexId]?.forEach { edge -> adjIn[edge.to]?.remove(edge) }
            adjIn[vertexId]?.forEach { edge -> adjOut[edge.from]?.remove(edge) }

            adjIn.remove(vertexId)
            adjOut.remove(vertexId)
        }
    }

    fun removeEdge(idFrom: Int, idTo: Int) {
        if (adjOut.contains(idFrom)) {
            val edge = getEdgeFromVertexId(idFrom, idTo)
            if (edge != null) {
                adjOut[idFrom]?.remove(edge)
                adjIn[idTo]?.remove(edge)
            }
        }
    }

    private fun getEdgeFromVertexId(from: Int, to: Int): Edge? {
        if (!adjOut.containsKey(from) || !adjIn.containsKey(to))
            return null

        adjOut[from]?.forEach { edge ->
            if (edge.to == to)
                return edge
        }
        if (!isDirected)
            adjIn[from]?.forEach { edge ->
                if (edge.from == to)
                    return edge
            }

        return null
    }

    fun edges(outbound: Int): Iterable<Edge> {
        return object : Iterable<Edge> {
            override fun iterator(): Iterator<Edge> {
                return object : Iterator<Edge> {
                    internal var iterateInbound = !isDirected
                    internal var iterator = adjOut[outbound]!!.iterator()
                    internal var list: List<Edge> = mutableListOf()
                    internal var listIndex = -1
                    override fun hasNext(): Boolean {
                        if (!iterator.hasNext()) {
                            if (iterateInbound) {
                                iterator = adjIn[outbound]!!.iterator()
                                iterateInbound = false
                                return hasNext()
                            } else {
                                return false
                            }
                        }
                        return true
                    }

                    override fun next(): Edge {
                        return iterator.next()
                    }

                    fun remove() {
                        throw UnsupportedOperationException()
                    }
                }
            }
        }
    }

    fun edges() : Iterator<Edge>{
        return object :Iterator<Edge>{
            internal val mapIterator = adjOut.keys.iterator()
            internal var list: MutableList<Edge>? = mutableListOf<Edge>()
            internal var listIndex = -1

            override fun hasNext(): Boolean {
                if (++listIndex >= list!!.size) {
                    if (mapIterator.hasNext()) {
                        list = adjOut!![mapIterator.next()]
                        listIndex = -1
                        return hasNext()
                    } else {
                        return false
                    }
                }
                return true
            }

            override fun next(): Edge {
                return list!![listIndex]
            }
        }
    }

    fun changeEdgeWeight(from: Int, to: Int, weight: Int) {
        val edge = getEdgeFromVertexId(from, to)
        edge?.weight = weight
    }

    fun getVertexes() = adjIn.keys

    fun neighbourVertexes(vertex: Int): List<Edge> {
        val result = mutableListOf<Edge>()

        result.addAll(adjOut[vertex]!!)
        if (!isDirected)
            result.addAll(adjIn[vertex]!!)

        return result
    }

    fun neighbourEdges(vertex: Int): List<Edge>{
        val result = mutableListOf<Edge>()

        result.addAll(adjOut[vertex]!!)
        if (!isDirected)
            result.addAll(adjIn[vertex]!!)
        return result
    }

    fun hasEulerianPath(graphModel: GraphModel): Boolean {
        val condition = adjOut.keys.map { vertex -> neighbourEdges(vertex) }
                .map { it.count() }
                .filter { it % 2 != 0 }
                .count()
        return condition == 2 || condition == 0
    }
}
